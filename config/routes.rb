Rails.application.routes.draw do
  get '/todos' => 'static_pages#todos'
  get '/todos/show' => 'static_pages#show'
  get 'todos/edit' => 'static_pages#edit'
  get 'todos/new' => 'static_pages#new'
	root 'application#hello'
end
